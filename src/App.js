import React from 'react';
import {HashRouter, Redirect, Route, Link} from 'react-router-dom';

import languages from './languages';
import Lesson from './components/Lesson';
import TopLevelNav from './components/TopLevelNav';
import BottomNav from './components/BottomNav'
import Menu from './components/Menu';

import './App.css';
import './menu.css';

function LanguageSelect({page, locale}) {
    return <div id="lang-select">
        {languages.map(
            language =>
                <div key={language.locale} className="lang-option">
                    <Link to={`/${language.locale}/${page}`}>
                        {language.name}
                    </Link>
                </div>
        )}
    </div>
}

export default function App() {
    return <HashRouter>
        <div className="wrapper">
            <Route exact path='/' render={() => (
                <Redirect to='/en/intro' />
            )}/>

            <input id="menu-tgl" type="checkbox"/>
            <label className="modal-background" htmlFor="menu-tgl"></label>

            <input id="lang-tgl" type="checkbox" />
            <label className="lang-modal-background" htmlFor="lang-tgl"></label>

            <header>
                <div id="tr-box">
                    <label className="lang-btn" htmlFor="lang-tgl">
                        <i className="fas fa-globe"></i>
                    </label>
                </div>
                <Link id="logo" to="/">
                    <i className="fas fa-bolt"></i>
                    <span>Way To Go</span>
                </Link>
                <label className="menu-btn" htmlFor="menu-tgl">
                    <i className="fas fa-bars"></i>
                </label>
            </header>

            <Route path="/:locale/:page" render={
                props => <LanguageSelect
                    locale={props.match.params.locale}
                    page={props.match.params.page} />
            } />

            <Route path="/:locale/:page" render={
                props => <TopLevelNav
                    locale={props.match.params.locale}
                    page={props.match.params.page} />
            } />
            <Route path="/:locale/:page" render={
                props => <Menu
                    locale={props.match.params.locale}
                    page={props.match.params.page} />
            } />

            <div id="content">
                <Route path="/:locale/:page" render={
                    props => <Lesson
                        locale={props.match.params.locale}
                        page={props.match.params.page} />
                } />
            </div>

            <footer>
                <Route path="/:locale/:page" render={
                    props => <BottomNav
                        locale={props.match.params.locale}
                        page={props.match.params.page} />
                } />
                <div className="clear"></div>
            </footer>
            <div className="sub-footer">
                <a href="https://gitlab.com/way-to-go/way-to-go.gitlab.io"
                    className="project-link">
                    <i className="fab fa-gitlab"></i> way-to-go
                </a>
                &mdash; Powered by
                <a href="https://duckpunch.org" className="personal-link">
                    duckpunch
                </a>
            </div>

        </div>
    </HashRouter>;
}
