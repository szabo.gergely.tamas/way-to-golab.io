const languages = [
    {
        name: 'English',
        locale: 'en',
        translators: [
            {
                name: 'Hiroki Mori',
            },
        ],
    },
    {
        name: 'Italiano',
        locale: 'it',
        translators: [
            {
                name: 'Cristian',
                link: 'https://gitlab.com/Filippone',
            },
        ],
    },
    {
        name: '日本語',
        locale: 'ja',
        translators: [
            {
                name: 'retas',
                link: 'https://gitlab.com/kaaretas',
            },
        ],
    },
    {
        name: 'Српски',
        locale: 'sr',
        translators: [
            {
                name: 'Milos Stankovic',
                link: 'https://gitlab.com/morph-dev',
            },
        ],
    },
];

export default languages;
