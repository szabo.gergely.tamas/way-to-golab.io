### Kakari e Shimari - gli angoli sono profittevoli

Imparerai come giocare nella fase inziale della partita - specialmente, la
negoziazione intorno agli angoli.

Puoi osservare come le partite reali inziano su questo goban 13x13. Clicca sulla
freccia di destra ripetutamente. Puoi tornare indietro con la freccia di sinistra.
Per prima cosa, ogni giocatore gioca in prossimità degli angoli.

La prima mossa di nero è chiamata 3-3 (o san-san) perchè si trova sulla terza linea
verticale e orizzontale iniziando a contare dall'angolo più vicino.
Questa mossa garantisce una posizione che può ottenere l'angolo.

Nella mossa successiva, anche bianco gioca sul 3-3.

La mossa seguente di nero è chiamato 4-4 (o hoshi) che è segnata con un piccolo cerchio nero.
Questa mossa può circondare un angolo più largo di una mossa in 3-3. Tuttavia, può essere
invaso molto facilmente perchè c'è più spazio tra essa e il bordo del goban.

La quarta mossa giocata da bianco è anch'essa una mossa popolare.

Ovviamente, è possibile giocare ovunque sul goban se non è una mossa illegale. Di solito,
in una partita si parte dagli angoli, ci si estende sui lati e successivamente ci si muove
verso il centro.

Nonostante gli angoli siano il posto migliore per fare territorio, nessuno giocherebbe una mossa
come M2 o N1 perchè sarebbe troppo piccola.

Board::corners0

---

Continuando dalla figura precedente...

Con la quinta mossa, nero rinforza il suo angolo in basso a sinistra giocando in F3.
Questo tipo di mosse di rinforzo sono chiamate Shimari ( o mosse di "chiusura").

Con la mossa seguente, anche bianco rinforza il suo angolo in basso a destra con un
tipo differente di shimari.

Successivamente, nero poggia un pietra vicino alla pietra bianca. (J10) Questa mossa implica che
se bianco la ignora, nero invaderà l'angolo giocando qualcosa come L11.
Questo tipo di mossa di approccio è chiamato Kakari (attaccare o minacciare).

A questo punto bianco "tocca" la pietra nera giocando J11 per difendersi. Dopo di che,
entrambi i giocatori si rinforzano rispettivamente. (K11,G10)

Bianco a questo punto potrebbe pensare che il lato sinistro del goban stia diventando territorio nero.
Per cui decide di invaderlo lui stesso per previre questa eventualità! Queste mosse sono chiamate
Uchikomi (distruggere, colpire o invadere) - A proposito, per ora non preoccuparti se non riesci a 
ricordare queste parole giapponesi. 

Questo è un esempio che mostra come iniziano le partite.

Tieni a mente che il Go è un gioco di condivisione - non puoi avere tutto. Devi cedere, devi
sacrificare, devi scusarti se sei stato troppo avido. Se diventi troppo aggressivo, perderai tutto.
Letteralmente!

Board::corners1
