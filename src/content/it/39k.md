A volte uno spazio interno può sembrare un occhio, ma in realtà non lo è.

Nero ha due occhi? Puoi giocare liberamente.

Board::free-falseeye

Nota che nero deve o riempire il suo occhio oppure giocare altrove permettendo a bianco
di catturare una pietra. Nero non ha due occhi qui - un occhio è un **occhio falso**

---

Evita che bianco possa forzare nero ad avere un falso occhio.

Board::stop-falseeye

---

Entrambi gli occhi di nero non sono ancora veri

Board::make-two-real

---

Nero è quasi circondato. Assicurati che abbia due occhi.

Board::edge-falseeye

---

E' spesso difficile per i principianti giudicare quando un occhio è finto o reale.
Con un po' di pratica, diventerà una cosa naturale.

