Nero è bloccato nell'angolo in basso a sinistra. Il gruppo verrà circondato se
non fai nulla. Tuttavia, il gruppo sarà al sicuro se catturi una delle pietre bianche.

Board::kamitori

Se nero ha successo, le sue pietre diventeranno abbastanza sicure avendo abbastanza spazio, 
o *occhi*, dove bianco non può giocare.

---

Il gruppo bianco in basso a destra non ha ancora due occhi. Tuttavia,
se le pietre segnate vengono catturate, bianco vivrà facilmente.

Assicurati che non succeda!

Board::oiotoshi

Se nero gioco nella maniera sbagliata, non si potrà far nulla per salvare le pietre.

Anche se connetti, finirai comunque per perdere le tue pietre. Questo sfortunata
posizione è chiamata *oiotoshi*.

Devi scappare *prima* dell'atari!

