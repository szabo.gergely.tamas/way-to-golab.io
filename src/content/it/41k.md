A volte puoi sacrificare una pietra per catturare il gruppo dell'avversario.
Questa tecnica è chiamata **snapback**.

Prova a catturare le 4 pietre bianche cercando di sacrificare una pietra.

Board::snapback-1

---

Questo è simile al problema precedente. Nota che se nero ha successo, tutte
le pietre nere saranno effettivamente connesse.

Board::snapback-2

---

Gli **Snapbacks** possono anche verificarsi lungo il bordo.

Board::snapback-3
