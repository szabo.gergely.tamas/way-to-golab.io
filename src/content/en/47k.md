In certain positions, black can make two simultaneous ataris for white.
Because white can't save both, black can capture at least one white stone.

This is called **double atari**.

---

White has too many weak points to shut black into the upper left corner.

Board::double-atari-1

---

Where can you make double atari?

Board::double-atari-2
