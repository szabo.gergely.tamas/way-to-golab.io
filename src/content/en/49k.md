## A bit more difficult

Stones stuck at the edges require fewer opposing stones to be completely
surround and are therefore easier to capture.

Try capturing the white stones that are in **atari**.

Board::capture1

---

A stone in the corner is in **atari**.

Board::capture2

---

Now, two white stones are in **atari**.

Board::capture3

---

Master these concepts and you shall be awarded the level of **49 Kyu**!
