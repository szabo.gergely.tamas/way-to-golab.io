Here, it looks as though white has the larger territory.

However, her wall has a weak point!  Try to "cut" white's wall.

Board::cut-1

This shows that it is possible to cut stones that are diagonally connected.

If stones are cut into two parts, each side has to live separately.  This is
more difficult than just living with one group!

---

Black has 30 points and white has 31 points.  It looks like it will be a 1
point win for white.

However, white has a serious weakness!

Cut white's wall and see what happens!

Board::cut-2
