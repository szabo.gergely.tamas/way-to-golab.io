## Before you begin

Remember the following 3 rules.

* Two players (black and white) take turns placing one stone on the board at a
  time.
* A stone must be placed on the intersection of the vertical and horizontal
  lines.
* Once a stone is placed, you can't move it, although under some conditions it
  may be removed.

Simple, right?

Now you understand half the rules of go.  Let's get started!

## The goal of Go

The objective of Go is to score more points than your opponent.

One way to make points is by **capturing** your opponent's stones.

Stones that are completely surrounded are removed from the board and are handed
to the opponent as prisoners.  Each prisoner is worth one point.

## Capturing Stones

Board::atari

With one more move, the white stone will be completely surrounded and captured.

Stones that can be removed with one more move are said to be in **atari**.

---

Board::captured

The white stone is now surrounded. Therefore...

---

Board::ponnuki

...it is removed from the board.

---

Board::escape

If it were white's turn, white could "escape" by connecting another stone.
