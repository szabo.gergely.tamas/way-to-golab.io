When you are 40 kyu, you should be able to solve problems that are more difficult.

Good luck!

Capture 3 white stones.

Board::cranes-nest

This is a famous shape called the "Crane's nest".  It can be really exciting if
you manage to play it in a game :)

---

Are the black stones surrounding or being surrounded?

Capture white before you get captured!

Board::capture-race

Note that this is a capturing race!
