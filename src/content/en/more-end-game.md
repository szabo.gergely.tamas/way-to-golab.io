Now back to 9x9 boards.

Board::more-end-game-1

Is this the end?  It looks like white has a big win.  However...

---

If you surround too large of an area, you run the risk of your opponent
invading.

Board::more-end-game-2

Here, it may be possible for black to make a live group inside white's
territory.

While this may be difficult due to white's wall, white's territory would be
drastically reduced if black is successful.

---

Board::more-end-game-3

Although it's far from the endgame, white has better prospects.  Can you see
why?
